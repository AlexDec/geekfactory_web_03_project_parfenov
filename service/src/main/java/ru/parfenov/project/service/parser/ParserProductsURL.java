package ru.parfenov.project.service.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;

public class ParserProductsURL {
    private Page page;
    private Pagination pagination;
    private List<String> productsURL;

    ParserProductsURL() {
        productsURL = new LinkedList<>();
    }

    public List<String> getProductsURL(Page starterPage) throws ParsingException {
        this.page = starterPage;
        if (page == null) {
            throw new ParsingException("Стартовая страница для парсинга ссылок на товары null");
        }
        pagination = new Pagination(page);
        boolean goNext = true;
        while (goNext) {
            getURLS();
            goNext = pagination.hasNextPage();
            if (goNext) {
                this.page = pagination.getNextPage();
                pagination.setPage(this.page);
            }
        }
        return productsURL;
    }

    private void getURLS() {
        Document document = page.getDocument();
        Elements productA = document.getElementsByClass("sel-product-tile-title");
        for (Element productLink :
                productA) {
            String link = productLink.attr("href");
            if (link.contains("/products/")) {
                productsURL.add(link);
            }
        }
    }

}
