package ru.parfenov.project.dao.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Table(name = "t_product")
public class ProductModel implements DaoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long productID;
    @Column
    private String name;
    @Column
    private String annotation;
    @Column
    private String description;
    @Column
    private String keywords;
    @Column
    private BigDecimal price;
    @Column
    private Double discount;
    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "is_popular")
    private Boolean isPopular;

    @OneToMany(mappedBy = "productModel")
    private List<ImagesModel> images;

    @ManyToMany
    @JoinTable(name = "t_products_categories",
            joinColumns = @JoinColumn(name = "id_category"),
            inverseJoinColumns = @JoinColumn(name = "id_product"))
    private List<CategoryModel> categoryID;
    @ManyToMany
    @JoinTable(name = "t_products_properties",
            joinColumns = @JoinColumn(name = "id_product"),
            inverseJoinColumns = @JoinColumn(name = "id_properties"))
    private List<PropertiesModel> properties;
}
