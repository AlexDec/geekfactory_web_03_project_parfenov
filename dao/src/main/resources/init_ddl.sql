CREATE TABLE IF NOT EXISTS t_product
(
    id          BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name        VARCHAR(100) UNIQUE               NOT NULL,
    annotation  VARCHAR(100),
    description VARCHAR(10000),
    keywords    VARCHAR(100),
    price       DECIMAL DEFAULT 0,
    discount    DOUBLE  DEFAULT 0,
    is_active   BOOL                              NOT NULL,
    is_popular  BOOL                              NOT NULL
);
CREATE TABLE IF NOT EXISTS t_images
(
    id         BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_product BIGINT,
    path       VARCHAR(50) UNIQUE,
    CONSTRAINT id_product_fk FOREIGN KEY (id_product) REFERENCES t_product (id)
);
CREATE TABLE IF NOT EXISTS t_category
(
    id                 BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name               VARCHAR(50) UNIQUE                NOT NULL,
    description        VARCHAR(10000),
    is_discount        BOOL                              NOT NULL,
    is_active          BOOL                              NOT NULL,
    category_url       varchar(100)                      NOT NULL,
    parent_category_id BIGINT,
    CONSTRAINT parent_category_fk FOREIGN KEY (parent_category_id) REFERENCES t_category (id)
);
CREATE TABLE IF NOT EXISTS t_products_categories
(
    id          BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_product  BIGINT                            NOT NULL,
    id_category BIGINT                            NOT NULL,
    CONSTRAINT id_product_fk_cat FOREIGN KEY (id_product) REFERENCES t_product (id),
    CONSTRAINT id_category_fk_prod FOREIGN KEY (id_category) REFERENCES t_category (id)
);
CREATE TABLE IF NOT EXISTS t_page
(
    id                 BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name               VARCHAR(50) UNIQUE                NOT NULL,
    keywords           VARCHAR(100),
    annotation         VARCHAR(100),
    description        VARCHAR(10000),
    second_description VARCHAR(100),
    url                VARCHAR(100) UNIQUE,
    is_active          BOOL                              NOT NULL
);
CREATE TABLE IF NOT EXISTS t_properties
(
    id    BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name  VARCHAR(50)                       NOT NULL,
    value VARCHAR(100)
);
CREATE TABLE IF NOT EXISTS t_products_properties
(
    id            BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_product    BIGINT                            NOT NULL,
    id_properties BIGINT                            NOT NULL,
    CONSTRAINT id_product_fk_prop FOREIGN KEY (id_product) REFERENCES t_product (id),
    CONSTRAINT id_properties_fk_prod FOREIGN KEY (id_properties) REFERENCES t_properties (id)
);
CREATE TABLE IF NOT EXISTS t_roles
(
    id      BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name    VARCHAR(50) UNIQUE                NOT NULL,
    isAdmin BOOL                              NOT NULL
);
CREATE TABLE IF NOT EXISTS t_user
(
    id              BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    userName        VARCHAR(50) UNIQUE                NOT NULL,
    userLogin       VARCHAR(50) UNIQUE                NOT NULL,
    userPassword    VARCHAR(50)                       NOT NULL,
    userEmail       VARCHAR(50) UNIQUE                NOT NULL,
    telephoneNumber INT                               NOT NULL,
    id_role         BIGINT,
    CONSTRAINT id_role_fk FOREIGN KEY (id_role) REFERENCES t_roles (id)
);
CREATE TABLE IF NOT EXISTS t_order
(
    id      BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_user BIGINT                            NOT NULL,
    CONSTRAINT id_user_fk FOREIGN KEY (id_user) REFERENCES t_user (id)
);
CREATE TABLE IF NOT EXISTS t_orders_products
(
    id         BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_product BIGINT                            NOT NULL,
    id_order   BIGINT                            NOT NULL,
    CONSTRAINT id_product_fk_orders FOREIGN KEY (id_product) REFERENCES t_product (id),
    CONSTRAINT id_order_fk FOREIGN KEY (id_order) REFERENCES t_order (id)
);