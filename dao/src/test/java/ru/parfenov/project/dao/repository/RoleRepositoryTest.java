package ru.parfenov.project.dao.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.configuration.DaoConfig;
import ru.parfenov.project.dao.model.RoleModel;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@Transactional
@ContextConfiguration(classes = DaoConfig.class)
public class RoleRepositoryTest {

    @Autowired
    @Qualifier("roleRepo")
    private RepositoryEntity<RoleModel, Long> roleRepository;

    @Test
    @DisplayName("Testing findByID role")
    public void findByIDTest() {
        RoleModel roleModel = new RoleModel();
        roleModel.setName("testRole");
        roleModel.setIsAdmin(false);
        roleRepository.save(roleModel);

        assertEquals(roleModel, roleRepository.findById(roleModel.getId()));
    }


    @Test
    @DisplayName("Testing save role")
    public void saveInDBTest() {
        RoleModel roleModel = new RoleModel();
        roleModel.setName("RoleTest");
        roleModel.setIsAdmin(true);
        roleRepository.save(roleModel);

        assertEquals(roleModel, roleRepository.findById(roleModel.getId()));
    }

    @Test
    @DisplayName("Testing findAll role")
    public void findAllTest() {
        RoleModel roleModelTest = new RoleModel();
        RoleModel roleModelTest2 = new RoleModel();
        LinkedList<RoleModel> roleModels = new LinkedList<>();

        roleModelTest.setName("testRole");
        roleModelTest.setIsAdmin(false);
        roleRepository.save(roleModelTest);
        roleModels.add(roleModelTest);

        roleModelTest2.setName("test2Role");
        roleModelTest2.setIsAdmin(true);
        roleRepository.save(roleModelTest2);
        roleModels.add(roleModelTest2);

        assertEquals(roleModels, roleRepository.findAll());
    }

    @Test
    @DisplayName("Testing remove role")
    public void removeTest() {
        RoleModel roleModel = new RoleModel();
        roleModel.setName("Role");
        roleModel.setIsAdmin(false);
        roleRepository.save(roleModel);
        assertEquals(1, roleRepository.findAll().size());
        roleRepository.remove(roleModel.getId());
        assertNull(roleRepository.findById(roleModel.getId()));
    }

    @Test
    @DisplayName("Testing update role")
    public void updateTest() {
        RoleModel roleModel = new RoleModel();
        roleModel.setName("Role");
        roleModel.setIsAdmin(false);
        roleRepository.save(roleModel);

        roleModel.setName("RoleUpdated");
        roleRepository.update(roleModel);
        assertEquals(roleModel, roleRepository.findById(roleModel.getId()));
    }
}
