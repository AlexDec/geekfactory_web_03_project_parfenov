package ru.parfenov.project.service.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;

public class ParserCategoriesURL {
    private static final Page mainPage = new Page("https://www.mvideo.ru/");
    private List<String> categoriesURL;

    public void startParsingCategories() {
        Document document = mainPage.getDocument();
        categoriesURL = new LinkedList<>();
        Elements categoriesUL = document.getElementsByClass("header-nav-drop-down-list-item");
        for (Element elementUL : categoriesUL) {
            Elements links = elementUL.getElementsByTag("a");
            for (Element elementLI : links) {
                String link = elementLI.attr("href");
                categoriesURL.add(link);
            }
        }
    }

    public List<String> getCategoriesURL() throws ParsingException {
        if (categoriesURL == null) {
            throw new ParsingException("Список категорий пуст");
        } else {
            return categoriesURL;
        }
    }
}
