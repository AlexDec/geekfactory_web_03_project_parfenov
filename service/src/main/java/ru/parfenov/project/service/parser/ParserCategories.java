package ru.parfenov.project.service.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.parfenov.project.service.handlers.entity.Category;

import java.util.ArrayList;
import java.util.List;

public class ParserCategories {
    private Document document;

    public ParserCategories(Document document) {
        this.document = document;
    }

    public List<Category> getCategories() {
        Elements elementsByClass = document.getElementsByClass("c-breadcrumbs__item");
        List<Category> categories = new ArrayList<>();

        for (Element byClass : elementsByClass) {
            Elements aTag = byClass.getElementsByTag("a");
            for (Element atrA : aTag) {
                Category category = new Category();
                if (!categories.isEmpty()) {
                    category.setParentCategory(categories.get(categories.size() - 1));
                }
                String text = atrA.text();
                if (text.equals("Главная")) {
                    continue;
                }
                category.setName(text);
                category.setIsActive(true);
                category.setDescription("None");
                category.setIsDiscount(false);
                categories.add(category);
            }
        }
        return categories;
    }
}

