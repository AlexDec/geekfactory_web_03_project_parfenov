package ru.parfenov.project.service.handlers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.parfenov.project.service.Translit;
import ru.parfenov.project.service.handlers.behaviors.CrudDaoBehavior;
import ru.parfenov.project.service.handlers.entity.Category;
import ru.parfenov.project.service.handlers.entity.ServiceModel;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryHandler {

    @Autowired
    @Qualifier("CategBehavior")
    private CrudDaoBehavior behavior;

    private final Logger logger = Logger.getLogger(CrudDaoBehavior.class);

    public List<Category> getAllCategory() {
        List<ServiceModel> serviceModels = behavior.findAll();
        if (serviceModels.isEmpty()) {
            logger.error("Нет категорий");
        }
        List<Category> allCategory = serviceModels.stream().filter(c -> c != null).filter(c -> c instanceof Category)
                .map(Category.class::cast).collect(Collectors.toCollection(LinkedList::new));
        return allCategory;
    }

    public Category finbyId(Long id) {
        ServiceModel model = behavior.findById(id);
        if (model instanceof Category)
            return (Category) model;
        else {
            logger.error("Ошибка при касте выгруженного с базы объекта Category.class");
            throw new ClassCastException("Ошибка при касте выгруженного с базы объекта Category.class");
        }
    }

    public boolean update(Category category) {
        try {
            setCategoryTranslitUrl(category);
            behavior.update(category);
        } catch (Exception e) {
            logger.error("Ошибка при апдейте", e);
            return false;
        }
        return true;
    }

    public Category save(Category category) {
        try {
            Category byName = (Category) behavior.findByName(category.getName());
            if (byName == null) {
                setCategoryTranslitUrl(category);
                Long id = behavior.save(category);
                category.setCategoryID(id);
            } else {
                return byName;
            }
        } catch (Exception e) {
            logger.error("Ошибка при сохранении", e);
        }
        return null;
    }

    private void setCategoryTranslitUrl(Category category) {
        if (category.getCategoryURL() == null) {
            Translit translit = new Translit();
            String translitUrl = translit.translit(category.getName());
            category.setCategoryURL(translitUrl);
        }
    }
}
