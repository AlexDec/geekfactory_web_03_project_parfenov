package ru.parfenov.project.dao.repository;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.model.UsersModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class UsersRepository implements RepositoryEntity<UsersModel, Long> {
    private final Logger logger = Logger.getLogger(UsersRepository.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UsersModel findById(Long id) {
        UsersModel usersModel1 = entityManager.find(UsersModel.class, id);
        return usersModel1;
    }

    @Override
    public List<UsersModel> findAll() {
        List<UsersModel> resultList = entityManager.createQuery("SELECT u from UsersModel u", UsersModel.class).getResultList();
        return resultList;
    }

    @Override
    public void remove(Long id) {
        UsersModel usersModel = entityManager.find(UsersModel.class, id);
        entityManager.remove(usersModel);
    }

    @Override
    public Long save(UsersModel model) {
        try {
            entityManager.persist(model);
            logger.info("Insert new User with id = " + model.getUserID());
        } catch (Exception e) {
            logger.error(e);
        }
        return model.getUserID();
    }

    @Override
    public UsersModel findByName(String name) {
        TypedQuery<UsersModel> query = entityManager.createQuery("SELECT u FROM UsersModel u WHERE u.userName = :name", UsersModel.class);
        query.setParameter("name", name);

        return query.getSingleResult();
    }

    @Override
    public void update(UsersModel model) {
        try {
            entityManager.merge(model);
            logger.info("Update User with id= " + model.getUserID());
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public UsersModel findByLogin(String login) {
        TypedQuery<UsersModel> query = entityManager.createQuery("SELECT u FROM UsersModel u WHERE u.userLogin = :login", UsersModel.class);
        query.setParameter("login", login);

        return query.getSingleResult();
    }
}