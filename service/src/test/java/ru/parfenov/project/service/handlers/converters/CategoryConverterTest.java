package ru.parfenov.project.service.handlers.converters;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.service.configuration.ServiceConfig;
import ru.parfenov.project.service.handlers.entity.Category;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfig.class)
public class CategoryConverterTest {

    @Autowired
    private CategoryConverter categoryConverter;

    @Test
    @DisplayName("Тест на конвертацию Category в Service модель (ParentCateg=Null)")
    public void convertToServiceTest() {
        Category expected = getCategory(1L);

        CategoryModel categoryModel = getCategoryModel(1L);

        Category actual = categoryConverter.convertToService(categoryModel);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию Category в Service модель (ParentCateg=2L)")
    public void convertToServiceTestNotNullParent() {
        Category expected = getCategory(1L);
        expected.setParentCategory(getCategory(2L));

        CategoryModel categoryModel = getCategoryModel(1L);
        categoryModel.setParentCategory(getCategoryModel(2L));

        Category actual = categoryConverter.convertToService(categoryModel);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию Category в Dao модель (ParentCateg=Null)")
    public void convertToDaoTest() {
        CategoryModel expected = getCategoryModel(1L);
        Category category = getCategory(1L);

        CategoryModel actual = categoryConverter.convertToDao(category);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию Category в Dao модель (ParentCateg=2L)")
    public void convertToDaoTestNotNullParent() {
        CategoryModel expected = getCategoryModel(1L);
        expected.setParentCategory(getCategoryModel(2L));

        Category category = getCategory(1L);
        category.setParentCategory(getCategory(2L));

        CategoryModel actual = categoryConverter.convertToDao(category);


        assertEquals(expected, actual);
    }

    private Category getCategory(Long id) {
        Category category = new Category();
        category.setCategoryID(id);
        category.setName("CategoryName" + id);
        category.setCategoryURL("/categ" + id);
        category.setDescription("Category Description" + id);
        category.setIsActive(true);
        category.setIsDiscount(true);
        category.setParentCategory(null);
        return category;
    }

    private CategoryModel getCategoryModel(Long id) {
        CategoryModel category = new CategoryModel();
        category.setCategoryID(id);
        category.setName("CategoryName" + id);
        category.setCategoryURL("/categ" + id);
        category.setDescription("Category Description" + id);
        category.setIsActive(true);
        category.setIsDiscount(true);
        category.setParentCategory(null);
        return category;
    }
}
