package ru.parfenov.project.service.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import ru.parfenov.project.dao.configuration.DaoConfig;
import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.dao.model.ImagesModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.repository.RepositoryEntity;
import ru.parfenov.project.service.handlers.CategoryHandler;
import ru.parfenov.project.service.handlers.ImagesHandler;
import ru.parfenov.project.service.handlers.ProductHandler;
import ru.parfenov.project.service.handlers.behaviors.CrudDaoBehavior;
import ru.parfenov.project.service.handlers.converters.*;
import ru.parfenov.project.service.parser.Parser;
import ru.parfenov.project.service.parser.ParserHandler;
import ru.parfenov.project.service.parser.download.Downloader;
import ru.parfenov.project.service.parser.parserInfo.ParserInfo;
import ru.parfenov.project.service.parser.parserInfo.StateParserInfo;

import java.util.LinkedList;

@Configuration
@Import(DaoConfig.class)
public class ServiceConfig {

    @Bean
    public Parser parser() {
        return new Parser();
    }

    @Bean
    public ParserInfo parserInfo() {
        ParserInfo parserInfo = new ParserInfo();
        parserInfo.setProductsParserInfo(new LinkedList<>());
        parserInfo.setStateParserInfo(StateParserInfo.NOT_STARTED);
        return parserInfo;
    }

    @Bean
    public ParserHandler parserStarter() {
        return new ParserHandler();
    }

    @Bean
    public ProductConverter getProductHandler() {
        return new ProductConverter();
    }

    @Bean
    public ImagesHandler getImgHandler() {
        return new ImagesHandler();
    }

    @Bean
    public CategoryHandler getCategoryHandler() {
        return new CategoryHandler();
    }

    @Bean
    public ProductHandler getProdController() {
        return new ProductHandler();
    }

    @Bean
    public CategoryConverter getCategoryConverter() {
        return new CategoryConverter();
    }

    @Bean
    public ImageConverter getImgConverter() {
        return new ImageConverter();
    }

    @Bean
    public Converter getProdConverter() {
        return new ProductConverter();
    }

    @Bean
    public RoleConverter getRoleConverter() {
        return new RoleConverter();
    }

    @Bean("NoConstructor")
    @Scope("prototype")
    public Downloader getDownloader() {
        return new Downloader();
    }

    @Autowired
    @Qualifier("imageRepo")
    private RepositoryEntity<ImagesModel, Long> imagesRepository;
    @Autowired
    private ImageConverter imageConverter;
    @Autowired
    @Qualifier("prodRepo")
    private RepositoryEntity<ProductModel, Long> productRepository;
    @Autowired
    private ProductConverter productConverter;
    @Autowired
    @Qualifier("categRepo")
    private RepositoryEntity<CategoryModel, Long> categoryRepository;
    @Autowired
    private CategoryConverter categoryConverter;

    @Bean("ImageBehavior")
    public CrudDaoBehavior getImgBehavior() {
        return new CrudDaoBehavior(imagesRepository, imageConverter);
    }

    @Bean("ProductBehavior")
    public CrudDaoBehavior getProdBehavior() {
        return new CrudDaoBehavior(productRepository, productConverter);
    }

    @Bean("CategBehavior")
    public CrudDaoBehavior getCategBehavior() {
        return new CrudDaoBehavior(categoryRepository, categoryConverter);
    }
}
