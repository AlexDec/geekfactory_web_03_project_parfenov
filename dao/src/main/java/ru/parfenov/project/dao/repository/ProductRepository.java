package ru.parfenov.project.dao.repository;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.model.ProductModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class ProductRepository implements RepositoryEntity<ProductModel, Long> {
    private final Logger logger = Logger.getLogger(ProductRepository.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ProductModel findById(Long id) {
        ProductModel productModel = entityManager.find(ProductModel.class, id);
        return productModel;
    }

    @Override
    public List<ProductModel> findAll() {
        List<ProductModel> products = entityManager.createQuery("FROM ProductModel pm", ProductModel.class).getResultList();
        return products;
    }

    @Override
    public void remove(Long id) {
        ProductModel productModel = entityManager.find(ProductModel.class, id);
        entityManager.remove(productModel);
    }

    @Override
    public Long save(ProductModel model) {
        entityManager.persist(model);
        model.getImages();
        model.getProperties();
        model.getCategoryID();
        return model.getProductID();
    }

    @Override
    public void update(ProductModel model) {
        entityManager.merge(model);
    }

    @Override
    public ProductModel findByName(String name) {
        TypedQuery<ProductModel> query = entityManager.createQuery("SELECT pm FROM ProductModel pm WHERE pm.name = :name", ProductModel.class);
        try {
            ProductModel productModel = query.setParameter("name", name).getSingleResult();
            return productModel;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }
}
