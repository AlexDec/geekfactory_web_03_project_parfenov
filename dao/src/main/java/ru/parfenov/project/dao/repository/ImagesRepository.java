package ru.parfenov.project.dao.repository;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.model.ImagesModel;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class ImagesRepository implements RepositoryEntity<ImagesModel, Long> {
    private final Logger logger = Logger.getLogger(ProductRepository.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ImagesModel findById(Long id) {
        ImagesModel imagesModel = entityManager.find(ImagesModel.class, id);
        return imagesModel;
    }

    @Override
    public List<ImagesModel> findAll() {
        List<ImagesModel> images = entityManager.createQuery("FROM ImagesModel im", ImagesModel.class).getResultList();
        return images;
    }

    @Override
    public void remove(Long id) {
        ImagesModel imagesModel = entityManager.find(ImagesModel.class, id);
        entityManager.remove(imagesModel);
    }

    @Override
    public Long save(ImagesModel model) {
        entityManager.persist(model);
        return model.getImageID();
    }

    @Override
    public void update(ImagesModel model) {
        entityManager.merge(model);
    }

    @Override
    public ImagesModel findByName(String name) {
        TypedQuery<ImagesModel> query = entityManager.createQuery("SELECT i FROM ImagesModel i WHERE i.path=:path", ImagesModel.class);
        try {
            ImagesModel im = query.setParameter("path", name).getSingleResult();
            return im;
        } catch (NoResultException ex) {
            return null;
        }
    }
}
