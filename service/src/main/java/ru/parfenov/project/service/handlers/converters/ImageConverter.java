package ru.parfenov.project.service.handlers.converters;

import org.springframework.beans.factory.annotation.Autowired;
import ru.parfenov.project.dao.model.ImagesModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.service.handlers.entity.Image;

public class ImageConverter implements Converter<Image, ImagesModel> {
    @Autowired
    private ProductConverter productConverter;

    @Override
    public Image convertToService(ImagesModel daoModel) {
        Image image = new Image();
        image.setImageID(daoModel.getImageID());
        image.setPath(daoModel.getPath());

        return image;
    }

    @Override
    public ImagesModel convertToDao(Image serviceModel) {
        ImagesModel imagesModel = new ImagesModel();
        imagesModel.setImageID(serviceModel.getImageID());
        imagesModel.setPath(serviceModel.getPath());
        if (serviceModel.getProduct() != null) {
            ProductModel productModel = productConverter.convertToDao(serviceModel.getProduct());
            imagesModel.setProductModel(productModel);
        }
        return imagesModel;
    }
}
