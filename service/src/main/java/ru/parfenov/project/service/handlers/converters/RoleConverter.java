package ru.parfenov.project.service.handlers.converters;

import ru.parfenov.project.dao.model.RoleModel;
import ru.parfenov.project.service.handlers.entity.Role;

public class RoleConverter implements Converter<Role, RoleModel> {

    @Override
    public Role convertToService(RoleModel daoModel) {
        Role role = new Role();
        role.setRoleID(daoModel.getId());
        role.setName(daoModel.getName());
        role.setIsAdmin(daoModel.getIsAdmin());
        return role;
    }

    @Override
    public RoleModel convertToDao(Role serviceModel) {
        RoleModel roleModel = new RoleModel();
        roleModel.setId(serviceModel.getRoleID());
        roleModel.setName(serviceModel.getName());
        roleModel.setIsAdmin(serviceModel.getIsAdmin());
        return roleModel;
    }
}
