package ru.parfenov.project.service.handlers.entity;

import lombok.Data;

@Data
public class User implements ServiceModel {

    private Long userID;
    private String userName;
    private String userLogin;
    private String userPassword;
    private String userEmail;
    private Integer telephoneNumber;
    private Role role;
}
