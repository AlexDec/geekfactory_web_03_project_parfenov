package ru.parfenov.project.service.handlers.converters;

import org.springframework.beans.factory.annotation.Autowired;
import ru.parfenov.project.dao.model.RoleModel;
import ru.parfenov.project.dao.model.UsersModel;
import ru.parfenov.project.service.handlers.entity.Role;
import ru.parfenov.project.service.handlers.entity.User;

public class UserConverter implements Converter<User, UsersModel> {
    @Autowired
    private RoleConverter roleConverter;

    @Override
    public User convertToService(UsersModel daoModel) {
        User user = new User();
        user.setUserID(daoModel.getUserID());
        user.setUserName(daoModel.getUserName());
        user.setUserLogin(daoModel.getUserLogin());
        user.setUserPassword(daoModel.getUserPassword());
        user.setTelephoneNumber(daoModel.getTelephoneNumber());
        user.setUserEmail(daoModel.getUserEmail());

        Role role = roleConverter.convertToService(daoModel.getRole());
        user.setRole(role);

        return user;
    }

    @Override
    public UsersModel convertToDao(User serviceModel) {
        UsersModel usersModel = new UsersModel();
        usersModel.setUserID(serviceModel.getUserID());
        usersModel.setUserName(serviceModel.getUserName());
        usersModel.setUserLogin(serviceModel.getUserLogin());
        usersModel.setUserPassword(serviceModel.getUserPassword());
        usersModel.setTelephoneNumber(serviceModel.getTelephoneNumber());
        usersModel.setUserEmail(serviceModel.getUserEmail());

        RoleModel roleModel = roleConverter.convertToDao(serviceModel.getRole());
        usersModel.setRole(roleModel);

        return usersModel;
    }
}
