package ru.parfenov.project.dao.repository;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.model.RoleModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
public class RoleRepository implements RepositoryEntity<RoleModel, Long> {
    private final Logger logger = Logger.getLogger(RoleRepository.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public RoleModel findById(Long id) {
        RoleModel roleModel = entityManager.find(RoleModel.class, id);
        return roleModel;
    }

    @Override
    public List<RoleModel> findAll() {
        List<RoleModel> roleModels = entityManager.createQuery("from RoleModel r", RoleModel.class).getResultList();
        return roleModels;
    }

    @Override
    public void remove(Long id) {
        RoleModel roleModel = entityManager.find(RoleModel.class, id);
        entityManager.remove(roleModel);
    }

    @Transactional
    @Override
    public Long save(RoleModel model) {
        try {
            entityManager.persist(model);
            logger.info("Add RoleModel" + model.getId());
        } catch (Exception e) {
            logger.error(e);
        }
        return model.getId();
    }

    @Override
    public void update(RoleModel model) {
        try {
            entityManager.merge(model);
            logger.info("Update Role with id= " + model.getId());
        } catch (Exception ex) {
            logger.error(ex);
        }
    }
}
