package ru.parfenov.project.service.handlers.converters;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.service.configuration.ServiceConfig;
import ru.parfenov.project.service.handlers.entity.Category;
import ru.parfenov.project.service.handlers.entity.Product;

import java.math.BigDecimal;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = ServiceConfig.class)
@ExtendWith({MockitoExtension.class, SpringExtension.class})
public class ProductConverterTest {

    @Mock
    private CategoryConverter categoryConverter;
    @InjectMocks
    private ProductConverter productConverter;

    @Test
    @DisplayName("Тест на конвертацию Product в Service модель (Category == Null)")
    public void convertToServiceTest() {
        Product expected = getProduct(1L);

        ProductModel productModel = getProductModel(1L);
        Product actual = productConverter.convertToService(productModel);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию Product в Service модель (Category == 1L)")
    public void convertToServiceTestCategoryNotNull() {
        Product expected = getProduct(1L);
        Category category = getCategory(1L);

        LinkedList<Category> list = new LinkedList<>();
        list.add(category);
        expected.setCategoryID(list);

        when(categoryConverter.convertToService(any())).thenReturn(category);

        ProductModel productModel = getProductModel(1L);
        CategoryModel categoryModel = getCategoryModel(1L);
        LinkedList<CategoryModel> listModels = new LinkedList<>();
        listModels.add(categoryModel);
        productModel.setCategoryID(listModels);

        Product actual = productConverter.convertToService(productModel);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию Product в Dao модель (Category == Null)")
    public void convertToDaoTest() {
        ProductModel expected = getProductModel(1L);
        Product product = getProduct(1L);


        ProductModel actual = productConverter.convertToDao(product);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию Product в Dao модель (Category == 1L)")
    public void convertToServiceTestDaoNotNull() {
        ProductModel expected = getProductModel(1L);

        CategoryModel categoryModel = getCategoryModel(1L);

        LinkedList<CategoryModel> listModels = new LinkedList<>();
        listModels.add(categoryModel);
        expected.setCategoryID(listModels);

        when(categoryConverter.convertToDao(any())).thenReturn(categoryModel);


        Product product = getProduct(1L);
        Category category = getCategory(1L);
        LinkedList<Category> list = new LinkedList<>();
        list.add(category);
        product.setCategoryID(list);

        ProductModel actual = productConverter.convertToDao(product);

        assertEquals(expected, actual);
    }

    private Product getProduct(Long id) {
        Product product = new Product();
        product.setProductID(id);
        product.setName("ProductName" + id);
        product.setAnnotations("ProductAnnotation" + id);
        product.setDescription("ProductDescription" + id);
        product.setKeywords("ProductDescription" + id);
        product.setPrice(new BigDecimal(id));
        product.setDiscount(id.doubleValue());
        product.setIsActive(true);
        product.setIsPopular(true);
        return product;
    }

    private ProductModel getProductModel(Long id) {
        ProductModel product = new ProductModel();
        product.setProductID(id);
        product.setName("ProductName" + id);
        product.setAnnotation("ProductAnnotation" + id);
        product.setDescription("ProductDescription" + id);
        product.setKeywords("ProductDescription" + id);
        product.setPrice(new BigDecimal(id));
        product.setDiscount(id.doubleValue());
        product.setIsActive(true);
        product.setIsPopular(true);
        return product;
    }

    private Category getCategory(Long id) {
        Category category = new Category();
        category.setCategoryID(id);
        category.setName("CategoryName" + id);
        category.setCategoryURL("/categ" + id);
        category.setDescription("Category Description" + id);
        category.setIsActive(true);
        category.setIsDiscount(true);
        category.setParentCategory(null);
        return category;
    }

    private CategoryModel getCategoryModel(Long id) {
        CategoryModel category = new CategoryModel();
        category.setCategoryID(id);
        category.setName("CategoryName" + id);
        category.setCategoryURL("/categ" + id);
        category.setDescription("Category Description" + id);
        category.setIsActive(true);
        category.setIsDiscount(true);
        category.setParentCategory(null);
        return category;
    }
}
