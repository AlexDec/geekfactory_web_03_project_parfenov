package ru.parfenov.project.dao.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.configuration.DaoConfig;
import ru.parfenov.project.dao.model.CategoryModel;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoConfig.class)
public class CategoryRepositoryTest {

    @Autowired
    @Qualifier("categRepo")
    private RepositoryEntity<CategoryModel, Long> categoryRepository;

    @Test
    @DisplayName("Тестирование метода save")
    public void saveTest() {
        CategoryModel categoryModel = getCategoryModel(1L);
        Long aLong = categoryRepository.save(categoryModel);

        assertEquals(categoryModel.getCategoryID(), aLong);
        assertEquals(categoryModel, categoryRepository.findById(categoryModel.getCategoryID()));
    }

    @Test
    @DisplayName("Тестирование метода findById")
    public void findByIDTestTest() {
        CategoryModel expected = getCategoryModel(1L);
        categoryRepository.save(expected);

        assertEquals(expected, categoryRepository.findById(expected.getCategoryID()));
    }

    @Test
    @DisplayName("Тестирование метода remove")
    public void removeTest() {
        CategoryModel categoryModel = getCategoryModel(1L);
        categoryRepository.save(categoryModel);

        assertEquals(categoryModel, categoryRepository.findById(categoryModel.getCategoryID()));
        categoryRepository.remove(categoryModel.getCategoryID());

        assertNull(categoryRepository.findById(categoryModel.getCategoryID()));
    }

    @Test
    @DisplayName("Тестирование метода update")
    public void updateTest() {
        CategoryModel categoryModel = getCategoryModel(1L);
        categoryRepository.save(categoryModel);

        assertEquals(categoryModel, categoryRepository.findById(categoryModel.getCategoryID()));

        categoryModel.setName("categoryUpdated");
        categoryModel.setCategoryURL("/updatedUrl");
        categoryModel.setDescription("Аля-Улю");
        categoryModel.setIsActive(false);
        categoryModel.setIsDiscount(true);
        categoryRepository.update(categoryModel);

        assertEquals(categoryModel, categoryRepository.findById(categoryModel.getCategoryID()));
    }

    @Test
    @DisplayName("Проверка с родетельскими категориями")
    public void findAllCycleTest() {
        CategoryModel parentCategory = getCategoryModel(1L);
        categoryRepository.save(parentCategory);
        CategoryModel categoryModel = getCategoryModel(2L);
        categoryModel.setParentCategory(parentCategory);

        categoryRepository.save(categoryModel);

        assertEquals(categoryModel, categoryRepository.findById(categoryModel.getCategoryID()));
    }

    @Test
    @DisplayName("Тестирование метода findAll")
    public void findAllTest() {
        List<CategoryModel> modelList = new LinkedList<>();
        CategoryModel categoryModel2 = getCategoryModel(1L);
        categoryRepository.save(categoryModel2);
        modelList.add(categoryModel2);

        CategoryModel categoryModel = getCategoryModel(2L);
        categoryRepository.save(categoryModel);
        modelList.add(categoryModel);
        assertEquals(modelList, categoryRepository.findAll());
    }

    private CategoryModel getCategoryModel(Long id) {
        CategoryModel category = new CategoryModel();
        category.setName("testCategory" + id);
        category.setDescription("testCategoryDescription" + id);
        category.setCategoryURL("/testCategoryURL" + id);
        category.setIsDiscount(false);
        category.setIsActive(true);
        category.setParentCategory(null);
        return category;
    }
}
