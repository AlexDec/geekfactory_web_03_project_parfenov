package ru.parfenov.project.service.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;

public class ParserImages {
    private List<String> imagesLink;
    private Document document;

    public ParserImages(Document document) {
        this.document = document;
    }

    public List<String> getImages() {
        imagesLink = new LinkedList<>();
        Elements elements = document.getElementsByClass("c-carousel__images");

        for (Element div : elements) {
            Elements a = div.getElementsByTag("a");
            for (Element element : a) {
                String link = element.attr("data-src");
                if (link != null) {
                    link.replace("//", "https://");
                    imagesLink.add(link);
                }
            }
        }
        return imagesLink;
    }
}
