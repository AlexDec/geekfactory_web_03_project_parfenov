package ru.parfenov.project.dao.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_images")
public class ImagesModel implements DaoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long imageID;
    @Column
    private String path;

    @ManyToOne
    @JoinColumn(name = "id_product")
    private ProductModel productModel;
}
