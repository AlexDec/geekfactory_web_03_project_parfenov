package ru.parfenov.project.service.handlers.behaviors;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.parfenov.project.dao.model.DaoModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.repository.RepositoryEntity;
import ru.parfenov.project.service.configuration.ServiceConfig;
import ru.parfenov.project.service.handlers.converters.Converter;
import ru.parfenov.project.service.handlers.entity.Product;
import ru.parfenov.project.service.handlers.entity.ServiceModel;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@ContextConfiguration(classes = ServiceConfig.class)
public class SaveBehaviorTest {

    @Mock
    private RepositoryEntity repository;
    @Mock
    private Converter<ServiceModel, DaoModel> converter;
    @InjectMocks
    private CrudDaoBehavior saveBehavior;

    @Test
    @DisplayName("Тест Product Save Behavior")
    public void convertToServiceTest() {
        ProductModel productModel = getProductModel(1L);

        when(converter.convertToDao(any())).thenReturn(productModel);
        when(repository.save(any())).thenReturn(1L);

        Product product = getProduct(1L);

        assertEquals(1L, saveBehavior.save(product));
    }

    private Product getProduct(Long id) {
        Product product = new Product();
        product.setProductID(id);
        product.setName("ProductName" + id);
        product.setAnnotations("ProductAnnotation" + id);
        product.setDescription("ProductDescription" + id);
        product.setKeywords("ProductDescription" + id);
        product.setPrice(new BigDecimal(id));
        product.setDiscount(id.doubleValue());
        product.setIsActive(true);
        product.setIsPopular(true);
        return product;
    }

    private ProductModel getProductModel(Long id) {
        ProductModel product = new ProductModel();
        product.setProductID(id);
        product.setName("ProductName" + id);
        product.setAnnotation("ProductAnnotation" + id);
        product.setDescription("ProductDescription" + id);
        product.setKeywords("ProductDescription" + id);
        product.setPrice(new BigDecimal(id));
        product.setDiscount(id.doubleValue());
        product.setIsActive(true);
        product.setIsPopular(true);
        return product;
    }

}
