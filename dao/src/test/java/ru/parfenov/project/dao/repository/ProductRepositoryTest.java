package ru.parfenov.project.dao.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.configuration.DaoConfig;
import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.dao.model.ImagesModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.dao.model.PropertiesModel;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Transactional
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoConfig.class)
public class ProductRepositoryTest {

    @Autowired
    @Qualifier("prodRepo")
    private RepositoryEntity<ProductModel, Long> productRepository;

    @Autowired
    @Qualifier("categRepo")
    private RepositoryEntity<CategoryModel, Long> categoryRepository;

    @Autowired
    @Qualifier("propRepo")
    private RepositoryEntity<PropertiesModel, Long> propertiesRepository;

    @Autowired
    @Qualifier("imageRepo")
    private RepositoryEntity<ImagesModel, Long> imagesRepository;

    @Test
    @DisplayName("Testing findByID product")
    public void findByIDTest() {
        ProductModel productModel = getProduct(1);
        CategoryModel categoryModel = getCategory(1);
        PropertiesModel propertiesModel = getProperties(1);
        ImagesModel imagesModel = getImages(1);

        productModel.getCategoryID().add(categoryModel);
        productModel.getProperties().add(propertiesModel);
        productModel.getImages().add(imagesModel);

        categoryRepository.save(categoryModel);
        propertiesRepository.save(propertiesModel);
        imagesRepository.save(imagesModel);
        productRepository.save(productModel);

        assertEquals(productModel, productRepository.findById(productModel.getProductID()));
    }

    @Test
    @DisplayName("Testing findByID with null Objects product")
    public void findByIDWithNullObjectsTest() {
        ProductModel productModel = getProduct(1);
        productRepository.save(productModel);

        assertEquals(productModel, productRepository.findById(productModel.getProductID()));
    }

    @Test
    @DisplayName("Testing findAll product")
    public void findAllTest() {
        ProductModel productModel = getProduct(1);
        ProductModel productModel2 = getProduct(2);

        productRepository.save(productModel);
        productRepository.save(productModel2);

        List<ProductModel> models = new LinkedList<>();
        models.add(productModel);
        models.add(productModel2);

        assertEquals(models, productRepository.findAll());

    }

    @Test
    @DisplayName("Testing save with new Objects product")
    public void saveProductWithNewObjectsTest() {
        ProductModel productModel = getProduct(1);
        CategoryModel categoryModel = getCategory(1);
        PropertiesModel propertiesModel = getProperties(1);
        ImagesModel imagesModel = getImages(1);

        productModel.getCategoryID().add(categoryModel);
        productModel.getProperties().add(propertiesModel);
        productModel.getImages().add(imagesModel);
        productRepository.save(productModel);

        assertEquals(productModel, productRepository.findById(productModel.getProductID()));
    }

    @Test
    @DisplayName("Тест на апдейт товара, без обновления категорий, свойств и картинок")
    public void updateProductTest() {
        ProductModel productModel = getProduct(1);
        CategoryModel categoryModel = getCategory(1);
        PropertiesModel propertiesModel = getProperties(1);
        ImagesModel imagesModel = getImages(1);

        productModel.getCategoryID().add(categoryModel);
        productModel.getProperties().add(propertiesModel);
        productModel.getImages().add(imagesModel);

        categoryRepository.save(categoryModel);
        propertiesRepository.save(propertiesModel);
        imagesRepository.save(imagesModel);
        productRepository.save(productModel);

        productModel.setName("updatedName");
        productModel.setAnnotation("updatedAnnotation");
        productRepository.update(productModel);

        assertEquals(productModel, productRepository.findById(productModel.getProductID()));
    }

    @Test
    @DisplayName("Тест на удаление товара")
    public void removeProductTest() {
        ProductModel productModel = getProduct(1);
        CategoryModel categoryModel = getCategory(1);
        PropertiesModel propertiesModel = getProperties(1);
        ImagesModel imagesModel = getImages(1);

        productModel.getCategoryID().add(categoryModel);
        productModel.getProperties().add(propertiesModel);
        productModel.getImages().add(imagesModel);

        categoryRepository.save(categoryModel);
        propertiesRepository.save(propertiesModel);
        imagesRepository.save(imagesModel);
        productRepository.save(productModel);

        assertEquals(categoryModel, categoryRepository.findById(categoryModel.getCategoryID()));
        assertEquals(propertiesModel, propertiesRepository.findById(propertiesModel.getPropertiesID()));
        assertEquals(imagesModel, imagesRepository.findById(imagesModel.getImageID()));

        productRepository.remove(productModel.getProductID());

        assertNull(imagesRepository.findById(imagesModel.getImageID()).getProductModel());
        assertEquals(categoryModel, categoryRepository.findById(categoryModel.getCategoryID()));
        assertEquals(propertiesModel, propertiesRepository.findById(propertiesModel.getPropertiesID()));
        assertEquals(imagesModel, imagesRepository.findById(imagesModel.getImageID()));
    }

    private ImagesModel getImages(int index) {
        ImagesModel imagesModel = new ImagesModel();

        imagesModel.setPath("testPath " + index);
        return imagesModel;
    }

    private ProductModel getProduct(int index) {
        ProductModel productModel = new ProductModel();

        productModel.setName("testName " + index);
        productModel.setAnnotation("testAnnotation " + index);
        productModel.setDescription("testDescription " + index);
        productModel.setKeywords("testKeywords " + index);
        productModel.setPrice(BigDecimal.valueOf(index));
        productModel.setDiscount((double) index);
        productModel.setIsActive(true);
        productModel.setIsPopular(true);

        productModel.setCategoryID(new LinkedList<>());
        productModel.setImages(new LinkedList<>());
        productModel.setProperties(new LinkedList<>());
        return productModel;
    }

    private PropertiesModel getProperties(int index) {
        PropertiesModel propertiesModel = new PropertiesModel();

        propertiesModel.setName("testName " + index);
        propertiesModel.setValue("testValue " + index);
        return propertiesModel;
    }

    private CategoryModel getCategory(int index) {
        CategoryModel categoryModel = new CategoryModel();

        categoryModel.setName("testCategory " + index);
        categoryModel.setDescription("testCategoryDescription " + index);
        categoryModel.setIsDiscount(false);
        categoryModel.setIsActive(true);
        categoryModel.setCategoryURL("/testCategoryURL " + index);
        categoryModel.setParentCategory(null);
        return categoryModel;
    }
}

