package ru.parfenov.project.service.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.parfenov.project.service.handlers.ProductHandler;
import ru.parfenov.project.service.handlers.entity.Product;

import java.math.BigDecimal;

public class ParserProducts {
    private ProductHandler productHandler;
    private Document document;
    private Product product;
    private Page page;

    ParserProducts(Page page) {
        this.page = page;
    }

    public ParserProducts(Document document) {
        this.document = document;
    }

    public Product parsingProduct() {
        if (page != null) {
            document = page.getDocument();
        }
        product = new Product();

        getNameProduct();
        getPrice();
        getDescription();
        product.setIsActive(true);
        product.setIsPopular(false);
        return product;
    }

    private void getDescription() {
        Elements divDescriptions = document.getElementsByClass("collapse-text-initial");
        for (Element element : divDescriptions) {
            product.setDescription(element.text());
        }
    }

    private void getPrice() {
        Elements divPrice = document.getElementsByClass("c-pdp-price__current sel-product-tile-price");
        BigDecimal bigDecimal = null;
        for (Element element : divPrice) {
            String priceString = element.text();
            priceString = priceString.replace("¤", "");
            priceString = priceString.replaceAll("\\s+", "");
            bigDecimal = new BigDecimal(priceString);
            product.setPrice(bigDecimal);
            break;
        }
    }

    private void getNameProduct() {
        Elements h1 = document.getElementsByClass("e-h1 sel-product-title");
        for (Element element : h1) {
            product.setName(element.text());
            break;
        }
    }
}
