package ru.parfenov.project.service.handlers.converters;

import ru.parfenov.project.dao.model.DaoModel;
import ru.parfenov.project.service.handlers.entity.ServiceModel;

public interface Converter<S extends ServiceModel, D extends DaoModel> {
    S convertToService(D daoModel);

    D convertToDao(S serviceModel);
}
