package ru.parfenov.project.service.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.parfenov.project.service.handlers.behaviors.CrudDaoBehavior;
import ru.parfenov.project.service.handlers.entity.Image;
import ru.parfenov.project.service.parser.download.DownloadException;
import ru.parfenov.project.service.parser.download.Downloader;

import java.util.LinkedList;
import java.util.List;

public class ImagesHandler {

    @Autowired
    @Qualifier("ImageBehavior")
    private CrudDaoBehavior behavior;

    @Autowired
    @Qualifier("NoConstructor")
    private Downloader downloader;

    public void saveImage(Image image) {
        Image byName = (Image) behavior.findByName(image.getPath());
        if (byName != null) {
            byName.setProduct(image.getProduct());
            behavior.update(byName);
        } else {
            behavior.save(image);
        }
    }

    private Image getImage(String name) {
        Image image = new Image();
        image.setPath(name);

        return image;
    }


    public List<Image> constructImagesObj(List<String> listImagesPath) throws DownloadException {
        List<Image> result = new LinkedList<>();
        for (String imgName : listImagesPath) {
            imgName = imgName.replace("//", "https://");
            downloader.downloadImageToLocalFile(imgName);
            imgName = imgName.substring(imgName.lastIndexOf("/")).replace("/", "");
            result.add(getImage(imgName));
        }
        return result;
    }
}
