package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.DaoModel;

import java.util.List;

public interface RepositoryEntity<T extends DaoModel, M> {

    T findById(M id);

    List<T> findAll();

    void remove(M id);

    Long save(T model);

    void update(T model);

    default T findByName(String name) {
        return null;
    }

}
