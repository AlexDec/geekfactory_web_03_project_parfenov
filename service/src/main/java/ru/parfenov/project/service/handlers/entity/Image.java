package ru.parfenov.project.service.handlers.entity;

import lombok.*;

@Getter
@Setter
@ToString(exclude = "product")
@EqualsAndHashCode(exclude = "product")
public class Image implements ServiceModel {
    private Long imageID;
    private String path;
    private Product product;
}
