package ru.parfenov.project.dao.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_user")
public class UsersModel implements DaoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long userID;
    @Column
    private String userName;
    @Column
    private String userLogin;
    @Column
    private String userPassword;
    @Column
    private String userEmail;
    @Column
    private Integer telephoneNumber;

    @ManyToOne
    @JoinColumn(name = "id_role")
    private RoleModel role;
}
