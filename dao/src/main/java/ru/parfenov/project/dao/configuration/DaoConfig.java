package ru.parfenov.project.dao.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.parfenov.project.dao.model.*;
import ru.parfenov.project.dao.repository.*;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DaoConfig {
    private final Logger logger = Logger.getLogger(DaoConfig.class);

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory());
        return jpaTransactionManager;
    }

    @Bean
    public Properties hibernateProperties() {
        Properties properties = new Properties();
        try {
            properties.load(Objects.requireNonNull(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("hibernate.properties")));
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Failed to load \"hibernate.properties\"");
        }
        return properties;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setPackagesToScan("ru.parfenov.project.dao.model", "");
        factoryBean.setJpaVendorAdapter(jpaVendorAdapter());
        factoryBean.setJpaProperties(hibernateProperties());
        factoryBean.afterPropertiesSet();
        return factoryBean.getNativeEntityManagerFactory();
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    @Bean("userRepo")
    public RepositoryEntity<UsersModel, Long> getUserRepo() {
        return new UsersRepository();
    }

    @Bean("roleRepo")
    public RepositoryEntity<RoleModel, Long> getRoleRepo() {
        return new RoleRepository();
    }

    @Bean("categRepo")
    public RepositoryEntity<CategoryModel, Long> getCategRepo() {
        return new CategoryRepository();
    }

    @Bean("imageRepo")
    public RepositoryEntity<ImagesModel, Long> getImgRepo() {
        return new ImagesRepository();
    }

    @Bean("prodRepo")
    public RepositoryEntity<ProductModel, Long> getProdRepo() {
        return new ProductRepository();
    }

    @Bean("propRepo")
    public RepositoryEntity<PropertiesModel, Long> getPropRepo() {
        return new PropertiesRepository();
    }
}
