package ru.parfenov.project.dao.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.configuration.DaoConfig;
import ru.parfenov.project.dao.model.PropertiesModel;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Transactional
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoConfig.class)
class PropertiesRepositoryTest {

    @Autowired
    @Qualifier("propRepo")
    RepositoryEntity<PropertiesModel, Long> propertiesRepository;

    @Test
    void findByIdTest() {
        PropertiesModel propertiesModel = getProperties(1);
        propertiesRepository.save(propertiesModel);

        assertEquals(propertiesModel, propertiesRepository.findById(propertiesModel.getPropertiesID()));
    }

    @Test
    void findAllTest() {
        PropertiesModel propertiesModel = getProperties(1);
        PropertiesModel propertiesModel2 = getProperties(2);
        List<PropertiesModel> properties = new LinkedList<>();
        propertiesRepository.save(propertiesModel);
        propertiesRepository.save(propertiesModel2);
        properties.add(propertiesModel);
        properties.add(propertiesModel2);

        assertEquals(properties, propertiesRepository.findAll());
    }

    @Test
    void removeTest() {
        PropertiesModel propertiesModel = getProperties(1);
        propertiesRepository.save(propertiesModel);
        assertEquals(propertiesModel, propertiesRepository.findById(propertiesModel.getPropertiesID()));

        propertiesRepository.remove(propertiesModel.getPropertiesID());
        assertNull(propertiesRepository.findById(propertiesModel.getPropertiesID()));
    }

    @Test
    void updateTest() {
        PropertiesModel propertiesModel = getProperties(1);
        propertiesRepository.save(propertiesModel);
        assertEquals(propertiesModel, propertiesRepository.findById(propertiesModel.getPropertiesID()));

        propertiesModel.setName("updatedName");
        propertiesRepository.update(propertiesModel);
        assertEquals(propertiesModel, propertiesRepository.findById(propertiesModel.getPropertiesID()));
    }

    private PropertiesModel getProperties(int index) {
        PropertiesModel propertiesModel = new PropertiesModel();
        propertiesModel.setName("TestName " + index);
        propertiesModel.setValue("TestValue " + index);
        return propertiesModel;
    }
}