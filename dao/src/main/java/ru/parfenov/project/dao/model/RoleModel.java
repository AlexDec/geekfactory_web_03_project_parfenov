package ru.parfenov.project.dao.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_roles")
public class RoleModel implements DaoModel {
    public static final Long DEFAULT = 1L;

    @Id
    @Column(columnDefinition = "NUMERIC(19,0)")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private Boolean isAdmin;

}
