package ru.parfenov.project.service.handlers.converters;

import org.springframework.beans.factory.annotation.Autowired;
import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.dao.model.ImagesModel;
import ru.parfenov.project.dao.model.ProductModel;
import ru.parfenov.project.service.handlers.entity.Category;
import ru.parfenov.project.service.handlers.entity.Image;
import ru.parfenov.project.service.handlers.entity.Product;

import java.util.LinkedList;
import java.util.List;

public class ProductConverter implements Converter<Product, ProductModel> {
    @Autowired
    private CategoryConverter categoryConverter;
    @Autowired
    private ImageConverter imageConverter;

    @SuppressWarnings("Duplicates")
    @Override
    public Product convertToService(ProductModel daoModel) {
        if (daoModel == null) {
            return null;
        }
        Product product = new Product();
        product.setProductID(daoModel.getProductID());
        product.setName(daoModel.getName());
        product.setAnnotations(daoModel.getAnnotation());
        product.setDescription(daoModel.getDescription());
        product.setKeywords(daoModel.getKeywords());
        product.setPrice(daoModel.getPrice());
        product.setDiscount(daoModel.getDiscount());
        product.setIsActive(daoModel.getIsActive());
        product.setIsPopular(daoModel.getIsPopular());

        List<CategoryModel> categoriesModel = daoModel.getCategoryID();
        if (categoriesModel != null && !categoriesModel.isEmpty()) {
            List<Category> categories = new LinkedList<>();
            for (CategoryModel categoryModel : categoriesModel) {
                categories.add(categoryConverter.convertToService(categoryModel));
            }
            product.setCategoryID(categories);
        }

        List<ImagesModel> imagesModels = daoModel.getImages();
        if (imagesModels != null && !imagesModels.isEmpty()) {
            List<Image> images = new LinkedList<>();
            for (ImagesModel imagesModel : imagesModels) {
                images.add(imageConverter.convertToService(imagesModel));
            }
            product.setImages(images);
        }

        return product;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ProductModel convertToDao(Product serviceModel) {
        if (serviceModel == null) {
            return null;
        }
        ProductModel productModel = new ProductModel();
        productModel.setProductID(serviceModel.getProductID());
        productModel.setName(serviceModel.getName());
        productModel.setAnnotation(serviceModel.getAnnotations());
        productModel.setDescription(serviceModel.getDescription());
        productModel.setKeywords(serviceModel.getKeywords());
        productModel.setPrice(serviceModel.getPrice());
        productModel.setDiscount(serviceModel.getDiscount());
        productModel.setIsActive(serviceModel.getIsActive());
        productModel.setIsPopular(serviceModel.getIsPopular());

        List<Category> categories = serviceModel.getCategoryID();
        if (categories != null && !categories.isEmpty()) {
            List<CategoryModel> categoryModels = new LinkedList<>();
            for (Category category : categories) {
                categoryModels.add(categoryConverter.convertToDao(category));
            }
            productModel.setCategoryID(categoryModels);
        }

        return productModel;
    }
}
