package ru.parfenov.project.dao.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.configuration.DaoConfig;
import ru.parfenov.project.dao.model.RoleModel;
import ru.parfenov.project.dao.model.UsersModel;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DaoConfig.class)
public class UserRepositoryTest {

    @Autowired
    @Qualifier("userRepo")
    private RepositoryEntity<UsersModel, Long> usersRepository;

    @Autowired
    @Qualifier("roleRepo")
    private RepositoryEntity roleRepository;

    @Test
    @DisplayName("Testing findByID user")
    public void findByIDTest() {
        UsersModel usersModel = getUsersModel();
        usersRepository.save(usersModel);

        assertEquals(usersModel, usersRepository.findById(usersModel.getUserID()));
    }

    @Test
    @DisplayName("Checking the default role setting")
    public void setDefaultRoleCheck() {
        UsersModel usersModel = new UsersModel();

        usersModel.setUserName("Tom");
        usersModel.setUserLogin("TomCat");
        usersModel.setTelephoneNumber(77777);
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setUserPassword("7778965");
        usersRepository.save(usersModel);

        assertEquals(usersModel, usersRepository.findById(usersModel.getUserID()));
    }

    @Test
    @DisplayName("Testing save user")
    public void createUserInDBTest() {
        UsersModel usersModel = new UsersModel();

        usersModel.setUserName("Tom");
        usersModel.setUserLogin("TomCat");
        usersModel.setTelephoneNumber(77777);
        usersModel.setUserEmail("Tom@mail.com");
        usersModel.setUserPassword("7778965");
        usersRepository.save(usersModel);

        assertEquals(usersModel, usersRepository.findById(usersModel.getUserID()));
    }

    @Test
    @DisplayName("Testing findAll users")
    public void findAllTest() {
        UsersModel usersModelTest1 = new UsersModel();
        UsersModel usersModelTest2 = new UsersModel();
        LinkedList<UsersModel> usersModels = new LinkedList<>();

        usersModelTest2.setUserName("testUser");
        usersModelTest2.setTelephoneNumber(796558);
        usersModelTest2.setUserLogin("testLogin");
        usersModelTest2.setUserEmail("testEmail");
        usersModelTest2.setUserPassword("testPassword");
        usersRepository.save(usersModelTest2);
        usersModels.add(usersModelTest2);

        usersModelTest1.setUserName("Tom");
        usersModelTest1.setTelephoneNumber(77777);
        usersModelTest1.setUserLogin("TomCat");
        usersModelTest1.setUserEmail("Tom@mail.com");
        usersModelTest1.setUserPassword("7778965");
        usersRepository.save(usersModelTest1);
        usersModels.add(usersModelTest1);

        assertEquals(usersModels, usersRepository.findAll());
    }

    @Test
    @DisplayName("Testing remove user")
    public void removeUserTest() {
        UsersModel usersModel = getUsersModel();
        usersRepository.save(usersModel);
        LinkedList<UsersModel> usersModels = new LinkedList<>();

        assertEquals(usersModel, usersRepository.findById(usersModel.getUserID()));

        usersRepository.remove(usersModel.getUserID());

        assertEquals(usersModels, usersRepository.findAll());
    }

    @Test
    @DisplayName("Testing update user")
    public void updateUserTest() {
        UsersModel usersModel = new UsersModel();

        usersModel.setUserName("User");
        usersModel.setTelephoneNumber(796558);
        usersModel.setUserLogin("Login");
        usersModel.setUserEmail("Email");
        usersModel.setUserPassword("Password");
        usersRepository.save(usersModel);
        assertEquals(usersModel, usersRepository.findById(usersModel.getUserID()));

        usersModel.setUserLogin("LoginUpdated");
        usersModel.setTelephoneNumber(123);
        usersRepository.update(usersModel);

        assertEquals(usersModel, usersRepository.findById(usersModel.getUserID()));
    }

    @Test
    @DisplayName("Test Save with new Role")
    public void saveWithNewRole() {
        UsersModel usersModel = getUsersModel();
        RoleModel roleModel = getRole();
        usersModel.setRole(roleModel);
        roleRepository.save(roleModel);
        usersRepository.save(usersModel);

        assertEquals(usersModel, usersRepository.findById(usersModel.getUserID()));
    }

    @Test
    @DisplayName("Testing findByName User")
    public void findByNameTest() {
        UsersModel usersModel = getUsersModel();
        usersRepository.save(usersModel);

        assertEquals(usersModel, usersRepository.findByName(usersModel.getUserName()));
    }

    private UsersModel getUsersModel() {
        UsersModel usersModelTest = new UsersModel();

        usersModelTest.setUserName("testUser");
        usersModelTest.setTelephoneNumber(796558);
        usersModelTest.setUserLogin("testLogin");
        usersModelTest.setUserEmail("testEmail");
        usersModelTest.setUserPassword("testPassword");
        return usersModelTest;
    }

    private RoleModel getRole() {
        RoleModel roleModel = new RoleModel();

        roleModel.setName("RoleTest");
        roleModel.setIsAdmin(false);
        return roleModel;
    }
}
