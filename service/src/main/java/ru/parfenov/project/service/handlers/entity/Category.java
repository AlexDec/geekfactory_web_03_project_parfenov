package ru.parfenov.project.service.handlers.entity;

import lombok.Data;

@Data
public class Category implements ServiceModel {
    private Long categoryID;
    private String name;
    private String description;
    private Boolean isDiscount;
    private Boolean isActive;
    private String categoryURL;
    private Category parentCategory;
}
