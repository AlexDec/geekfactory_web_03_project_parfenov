package ru.parfenov.project.dao.model;

import lombok.Data;

@Data
public class OrdersModel implements DaoModel {
    private Long ordersID;
    private UsersModel user;
    private Iterable<ProductModel> product;
}
