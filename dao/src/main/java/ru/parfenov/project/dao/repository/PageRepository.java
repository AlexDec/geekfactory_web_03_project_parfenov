package ru.parfenov.project.dao.repository;

import ru.parfenov.project.dao.model.PageModel;

import java.util.List;

public class PageRepository implements RepositoryEntity<PageModel, Long> {
    @Override
    public PageModel findById(Long id) {
        return null;
    }

    @Override
    public List<PageModel> findAll() {
        return null;
    }

    @Override
    public void remove(Long id) {

    }

    @Override
    public Long save(PageModel model) {
        return null;
    }

    @Override
    public void update(PageModel model) {

    }
}
