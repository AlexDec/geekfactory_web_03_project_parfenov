package ru.parfenov.project.service.parser.parserInfo;

import lombok.Data;

@Data
public class ProductParserInfo {
    private String productName;
    private ProductStateParserInfo productStateParserInfo;

    public ProductParserInfo(String name) {
        this.productName = name;
        this.productStateParserInfo = ProductStateParserInfo.UNDEFINED;
    }
}
