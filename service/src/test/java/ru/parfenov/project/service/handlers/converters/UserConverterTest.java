package ru.parfenov.project.service.handlers.converters;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.parfenov.project.dao.model.RoleModel;
import ru.parfenov.project.dao.model.UsersModel;
import ru.parfenov.project.service.configuration.ServiceConfig;
import ru.parfenov.project.service.handlers.entity.Role;
import ru.parfenov.project.service.handlers.entity.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfig.class)
@ExtendWith(MockitoExtension.class)
public class UserConverterTest {

    @Mock
    private RoleConverter roleConverter;

    @InjectMocks
    private UserConverter userConverter;

    @Test
    @DisplayName("Тест на конвертацию User в Service модель")
    public void convertToServiceTest() {
        Role role = getRole(1L);
        when(roleConverter.convertToService(any())).thenReturn(role);

        UsersModel userModel = getUsersModel(1L);
        User actual = userConverter.convertToService(userModel);
        User expected = getUser(1L);
        expected.setRole(role);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию User в DAO модель")
    public void convertToDaoTest() {
        RoleModel roleModel = getRoleModel(1L);
        when(roleConverter.convertToDao(any())).thenReturn(roleModel);

        User user = getUser(1L);
        UsersModel actual = userConverter.convertToDao(user);
        UsersModel expected = getUsersModel(1L);
        expected.setRole(roleModel);

        assertEquals(expected, actual);
    }

    private User getUser(Long id) {
        User user = new User();
        user.setUserID(id);
        user.setUserName("testUser" + id);
        user.setTelephoneNumber(id.intValue());
        user.setUserLogin("testLogin" + id);
        user.setUserEmail("testEmail" + id);
        user.setUserPassword("testPassword" + id);
        return user;
    }

    private UsersModel getUsersModel(Long id) {
        UsersModel user = new UsersModel();
        user.setUserID(id);
        user.setUserName("testUser" + id);
        user.setTelephoneNumber(id.intValue());
        user.setUserLogin("testLogin" + id);
        user.setUserEmail("testEmail" + id);
        user.setUserPassword("testPassword" + id);
        return user;
    }

    private Role getRole(Long id) {
        Role role = new Role();
        role.setRoleID(id);
        role.setName("testRole" + id);
        role.setIsAdmin(false);
        return role;
    }

    private RoleModel getRoleModel(Long id) {
        RoleModel role = new RoleModel();
        role.setId(id);
        role.setName("testRole" + id);
        role.setIsAdmin(false);
        return role;
    }
}
