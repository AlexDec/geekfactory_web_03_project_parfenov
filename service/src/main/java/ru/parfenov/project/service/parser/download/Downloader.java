package ru.parfenov.project.service.parser.download;

import jdk.nashorn.api.scripting.URLReader;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

@Getter
@Setter
public class Downloader {
    private URL url;
    private String html;

    public Downloader() {
    }

    public Downloader(URL url) {
        this.url = url;
    }

    public Downloader(String urlString) throws DownloadException {
        transformStringToURL(urlString);
    }

    public String download() throws DownloadException {
        return getHtml(url);
    }

    public String downloadImageToLocalFile(String href) {
        try {
            URL url = new URL(href);
            String fileName = url.getFile();
            String newImagePath = "..\\web\\src\\main\\webapp\\template\\images\\products" + fileName.substring(fileName.lastIndexOf("/"));
            System.out.println(newImagePath);

            InputStream inputStream = url.openStream();
            OutputStream outputStream = new FileOutputStream(newImagePath);

            byte[] buf = new byte[2048];
            int length = 0;

            while ((length = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, length);
            }

            inputStream.close();
            outputStream.close();
            fileName = fileName.substring(fileName.lastIndexOf("/"));
            fileName = fileName.replace("/", "");
            return fileName;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String download(URL url) throws DownloadException {
        return getHtml(url);
    }

    public String download(String urlString) throws DownloadException {
        transformStringToURL(urlString);
        return getHtml(url);
    }

    private String getHtml(URL url) throws DownloadException {
        this.url = url;
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new URLReader(url))) {
            line = bufferedReader.readLine();
            while (line != null) {
                line = bufferedReader.readLine();
                stringBuilder.append(line);
            }
            html = stringBuilder.toString();
            return html;
        } catch (IOException e) {
            throw new DownloadException("Невозможно скачать страницу");
        }
    }

    private void transformStringToURL(String urlString) throws DownloadException {
        try {
            this.url = new URL(urlString);
        } catch (MalformedURLException e) {
            throw new DownloadException("Не верный формат URL");
        }
    }
}
