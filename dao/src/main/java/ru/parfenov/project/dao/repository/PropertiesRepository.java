package ru.parfenov.project.dao.repository;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.model.PropertiesModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class PropertiesRepository implements RepositoryEntity<PropertiesModel, Long> {
    private final Logger logger = Logger.getLogger(ProductRepository.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PropertiesModel findById(Long id) {
        PropertiesModel propertiesModel = entityManager.find(PropertiesModel.class, id);
        return propertiesModel;
    }

    @Override
    public List<PropertiesModel> findAll() {
        List<PropertiesModel> properties = entityManager.createQuery("FROM PropertiesModel ppm",
                PropertiesModel.class).getResultList();
        return properties;
    }

    @Override
    public void remove(Long id) {
        PropertiesModel propertiesModel = entityManager.find(PropertiesModel.class, id);
        entityManager.remove(propertiesModel);
    }

    @Override
    public Long save(PropertiesModel model) {
        entityManager.persist(model);
        return model.getPropertiesID();
    }

    @Override
    public void update(PropertiesModel model) {
        entityManager.merge(model);
    }

    @Override
    public PropertiesModel findByName(String name) {
        TypedQuery<PropertiesModel> query = entityManager.createQuery("SELECT ppm FROM PropertiesModel ppm WHERE ppm.name= :name",
                PropertiesModel.class);
        try {
            PropertiesModel propertiesModel = query.setParameter("name", name).getSingleResult();
            return propertiesModel;
        } catch (Exception ex) {
            logger.error(ex);
        }
        return null;
    }
}
