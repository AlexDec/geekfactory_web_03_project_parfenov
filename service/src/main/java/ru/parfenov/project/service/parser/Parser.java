package ru.parfenov.project.service.parser;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import ru.parfenov.project.service.handlers.CategoryHandler;
import ru.parfenov.project.service.handlers.ImagesHandler;
import ru.parfenov.project.service.handlers.ProductHandler;
import ru.parfenov.project.service.handlers.entity.Category;
import ru.parfenov.project.service.handlers.entity.Image;
import ru.parfenov.project.service.handlers.entity.Product;
import ru.parfenov.project.service.parser.parserInfo.ParserInfo;
import ru.parfenov.project.service.parser.parserInfo.ProductParserInfo;
import ru.parfenov.project.service.parser.parserInfo.StateParserInfo;

import java.util.LinkedList;
import java.util.List;

public class Parser implements Runnable {
    private final Logger logger = Logger.getLogger(Parser.class);

    @Autowired
    private ParserInfo parserInfo;

    private List<String> categoriesURL;
    private List<String> productLinks;
    private ParserCategoriesURL parserCategoriesURL = new ParserCategoriesURL();

    @Autowired
    private ImagesHandler imagesHandler;
    @Autowired
    private ProductHandler productHandler;
    @Autowired
    private CategoryHandler categoryHandler;

    public Parser() {
    }

    @Autowired
    public Parser(ImagesHandler imagesHandler, ProductHandler productHandler, CategoryHandler categoryHandler) {
        this.imagesHandler = imagesHandler;
        this.productHandler = productHandler;
        this.categoryHandler = categoryHandler;
    }

    public void startParsing() {
        parserCategoriesURL.startParsingCategories();
        try {
            categoriesURL = parserCategoriesURL.getCategoriesURL();
        } catch (ParsingException e) {
            e.printStackTrace();
        }
        for (String link : categoriesURL) {
            synchronized (parserInfo) {
                if (parserInfo.getStateParserInfo().equals(StateParserInfo.DENY)) {
                    break;
                }
            }
            Page page = new Page(link);
            ParserProductsURL parserProductsURL = new ParserProductsURL();

            try {
                productLinks = parserProductsURL.getProductsURL(page);
            } catch (ParsingException e) {
                logger.info("Не преобразовался линк на категорию: " + link);
            }

            for (String productStringPage : productLinks) {
                synchronized (parserInfo) {
                    if (parserInfo.getStateParserInfo().equals(StateParserInfo.DENY)) {
                        break;
                    }
                }
                productParse(productStringPage);
            }
        }
        synchronized (parserInfo) {
            parserInfo.setStateParserInfo(StateParserInfo.END);
        }
    }

    private void productParse(String productStringPage) {
        Page productPage = new Page(productStringPage);
        Document document = productPage.getDocument();

        ParserProducts parserProducts = new ParserProducts(document);
        Product product = parserProducts.parsingProduct();

        parseCategImageSaveInProd(document, product);


    }

    private void parseCategImageSaveInProd(Document document, Product product) {
        ParserImages parserImages = new ParserImages(document);
        List<String> listImagesPath = parserImages.getImages();
        List<Image> images = imagesHandler.constructImagesObj(listImagesPath);


        ParserCategories parserCategories = new ParserCategories(document);
        List<Category> categories = parserCategories.getCategories();
        List<Category> updatedCategories = new LinkedList<>();

        product.setImages(images);
        for (Category category : categories) {
            updatedCategories.add(categoryHandler.save(category));
        }
        product.setCategoryID(updatedCategories);
        try {
            product = productHandler.saveProduct(product);
            parserInfo.getProductsParserInfo().add(new ProductParserInfo(product.getName()));
        } catch (Exception ex) {
            logger.error(ex);
        }

        for (Image image : images) {
            image.setProduct(product);
            imagesHandler.saveImage(image);
        }
    }

    @Override
    public void run() {
        parserInfo.setStateParserInfo(StateParserInfo.IN_PROGRESS);
        startParsing();
    }
}
