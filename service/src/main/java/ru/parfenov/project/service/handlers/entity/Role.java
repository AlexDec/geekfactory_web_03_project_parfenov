package ru.parfenov.project.service.handlers.entity;

import lombok.Data;

@Data
public class Role implements ServiceModel {
    private Long roleID;
    private String name;
    private Boolean isAdmin;
}
