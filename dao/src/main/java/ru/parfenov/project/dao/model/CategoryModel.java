package ru.parfenov.project.dao.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_category")
public class CategoryModel implements DaoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long categoryID;
    @Column
    private String name;
    @Column
    private String description;
    @Column(name = "is_discount")
    private Boolean isDiscount;
    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "category_url")
    private String categoryURL;

    @ManyToOne()
    @JoinColumn(name = "parent_category_id")
    private CategoryModel parentCategory;
}
