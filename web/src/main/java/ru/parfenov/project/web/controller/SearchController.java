package ru.parfenov.project.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.parfenov.project.service.handlers.ProductHandler;
import ru.parfenov.project.service.handlers.entity.Product;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    ProductHandler productHandler;

    @GetMapping()
    public String postCategoryQuery(@RequestParam("search") String input, Model model) {
        List<Product> productList = productHandler.findProductContains(input);

        model.addAttribute("search", input);
        model.addAttribute("products", productList);
        return "category";
    }
}
