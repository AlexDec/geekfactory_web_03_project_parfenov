package ru.parfenov.project.service;

import com.ibm.icu.text.Transliterator;

public class Translit {
    private static final String CYRILLIC_TO_LATIN = "Cyrillic-Latin";

    public String translit(String inputKiril) {
        Transliterator toLatinTrans = Transliterator.getInstance(CYRILLIC_TO_LATIN);
        String result = toLatinTrans.transliterate(inputKiril);
        result.replace(" ", "-");
        return result;
    }
}
