package ru.parfenov.project.service.parser;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ru.parfenov.project.service.parser.parserInfo.ParserInfo;
import ru.parfenov.project.service.parser.parserInfo.StateParserInfo;

import java.util.Set;

public class ParserHandler {
    private final Logger logger = Logger.getLogger(ParserHandler.class);
    @Autowired
    private Parser parser;

    @Autowired
    private ParserInfo parserInfo;

    private Thread thread;

    public void startParsing() {
        Thread parserThread = new Thread(parser, "Parser");
        parserThread.start();
    }

    public void denyParser() {
        parserInfo.setStateParserInfo(StateParserInfo.DENY);
    }

    public boolean waitParsing() {
        if (thread == null) {
            findThread();
        }
        try {
            thread.wait();
            parserInfo.setStateParserInfo(StateParserInfo.WAIT);
        } catch (InterruptedException e) {
            logger.error(e);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void findThread() {
        Set<Thread> threads = Thread.getAllStackTraces().keySet();
        for (Thread thread : threads) {
            if (thread.getName().equals("Parser")) {
                this.thread = thread;
            }
        }
    }

    public boolean resume() {
        parserInfo.setStateParserInfo(StateParserInfo.IN_PROGRESS);
        thread.notify();
        return true;
    }
}
