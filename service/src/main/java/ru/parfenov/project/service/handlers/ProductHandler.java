package ru.parfenov.project.service.handlers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.parfenov.project.dao.repository.DaoLayerException;
import ru.parfenov.project.service.handlers.behaviors.CrudDaoBehavior;
import ru.parfenov.project.service.handlers.entity.Product;
import ru.parfenov.project.service.handlers.entity.ServiceModel;

import java.util.LinkedList;
import java.util.List;

public class ProductHandler {

    @Autowired
    @Qualifier("ProductBehavior")
    private CrudDaoBehavior behavior;

    private final Logger logger = Logger.getLogger(ProductHandler.class);

    public Product saveProduct(Product product) {
        Product byName = (Product) behavior.findByName(product.getName());
        if (byName != null) {
            byName.setCategoryID(product.getCategoryID());
            byName.setImages(product.getImages());
            byName.setProperties(product.getProperties());
            behavior.update(byName);
            return byName;
        }
        try {
            behavior.save(product);
            return product;
        } catch (DaoLayerException ex) {
            System.out.println("Ошибка записи в БД " + ex);
        } catch (Exception e) {
            System.out.println(e + " Ошибка при сохраненеии " + product.getName());
        }
        return null;
    }

    public ServiceModel findById(Long id) {
        ServiceModel resultProduct = behavior.findById(id);

        return resultProduct;
    }

    public List<Product> findProductContains(String partName) {
        if (partName == null) {
            return null;
        }

        List<ServiceModel> products = behavior.findAll();
        List<Product> resultList = new LinkedList();

        for (ServiceModel serviceModel : products) {
            if (serviceModel != null && serviceModel instanceof Product) {
                Product product = (Product) serviceModel;
                if (product != null && product.getName().contains(partName)) {
                    resultList.add(product);
                }
            } else {
                logger.error("Не верный формат при выгрузке из БД Products");
                throw new ClassCastException("Не верный формат при выгрузке из БД Products");
            }
        }
        return resultList;
    }

    public Product finById(Long id) {
        ServiceModel serviceModel = behavior.findById(id);

        if (serviceModel instanceof Product) {
            return (Product) serviceModel;
        }
        logger.error("Не верный формат при выгрузке из БД Products");
        throw new ClassCastException("Не верный формат при выгрузке из БД Products");
    }

    public boolean update(Product product) {
        try {
            behavior.update(product);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            return false;
        }
    }

    public List<Product> fidAll() {
        List<ServiceModel> serviceModels = behavior.findAll();
        List<Product> products = new LinkedList<>();
        for (ServiceModel model : serviceModels) {
            if (model instanceof Product) {
                products.add((Product) model);
            }
        }
        return products;
    }
}
