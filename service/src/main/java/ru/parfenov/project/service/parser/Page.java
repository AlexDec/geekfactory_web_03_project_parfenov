package ru.parfenov.project.service.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ru.parfenov.project.service.parser.download.DownloadException;
import ru.parfenov.project.service.parser.download.Downloader;

import java.net.URL;

public class Page {
    private URL url;
    private String html;
    private Downloader downloader;
    private String urlString;
    private Document document;

    Page(String urlString) {
        if (!urlString.contains("https://www.mvideo.ru/")) {
            urlString = "https://www.mvideo.ru" + urlString;
        }
        this.urlString = urlString;
        downloadHTML();
        this.document = Jsoup.parse(html);
    }

    Page(URL url) {
        this.url = url;
    }

    public URL getUrl() {
        return this.url;
    }

    public String getUrlString() {
        return this.urlString;
    }

    public String getHtml() {
        if (html != null) {
            return html;
        }
        downloadHTML();
        return html;
    }

    public Document getDocument() {
        return this.document;
    }

    private void downloadHTML() {
        try {
            downloader = new Downloader(urlString);
            downloader.download();
        } catch (DownloadException e) {
            e.printStackTrace();
        }
        html = downloader.getHtml();
    }

    public String getHtml(String urlString) {
        try {
            downloader = new Downloader(urlString);
            downloader.download();
        } catch (DownloadException e) {
            e.printStackTrace();
        }
        return downloader.getHtml();
    }
}
