package ru.parfenov.project.dao.model;

import lombok.Data;

@Data
public class PageModel implements DaoModel {
    private Long pageID;
    private String name;
    private String keywords;
    private String annotations;
    private String description;
    private String secondDescription;
    private String urlPage;
    private Boolean isActive;
}
