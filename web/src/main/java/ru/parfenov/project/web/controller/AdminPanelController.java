package ru.parfenov.project.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.parfenov.project.service.handlers.CategoryHandler;
import ru.parfenov.project.service.handlers.ProductHandler;
import ru.parfenov.project.service.handlers.entity.Category;
import ru.parfenov.project.service.handlers.entity.Product;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminPanelController {
    private final Logger logger = Logger.getLogger(AdminPanelController.class);
    @Autowired
    private CategoryHandler categoryHandler;

    @Autowired
    private ProductHandler productHandler;

    @GetMapping
    public String mainAdminPanel(Model model) {

        return "/admin/adminMain";
    }

    @GetMapping("/categories")
    public String categoryList(Model model) {
        List<Category> allCategories = categoryHandler.getAllCategory();
        model.addAttribute("categories", allCategories);

        return "/admin/adminCategories";
    }

    @GetMapping("/category/{id}")
    public String singleCategoryPage(@PathVariable("id") Long id, Model model) {

        Category category = categoryHandler.finbyId(id);
        model.addAttribute("category", category);
        return "/admin/adminCategory";
    }


    @PostMapping(value = "/category/{id}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public String singleCategoryPagePost(@PathVariable("id") Long id, @RequestBody Category category, Model model) {
        logger.info(category);

        categoryHandler.update(category);

        model.addAttribute("category", category);

        return "/admin/adminCategory";
    }

    @GetMapping(value = "/category-new")
    public String singleNewCategoryPage(Model model) {

        return "/admin/adminNewCategory";
    }

    @PostMapping(value = "/category-new")
    public String singleNewCategoryPagePost(@RequestBody Category category, Model model) {
        logger.info(category);
        categoryHandler.save(category);
        model.addAttribute("category", category);

        return "/admin/adminNewCategory";
    }

    @GetMapping("/products")
    public String productsList(Model model) {
        List<Product> products = productHandler.fidAll();
        model.addAttribute("products", products);

        return "/admin/adminProducts";
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public String singleProductPage(@PathVariable("id") Long id, Model model) {

        Product product = productHandler.finById(id);
        model.addAttribute("product", product);
        return "/admin/adminProduct";
    }

    @GetMapping(value = "/product-new")
    public String singleNewProductPage(Model model) {

        return "/admin/adminNewProduct";
    }

    @PostMapping(value = "/product-new")
    public String singleNewProductPagePost(@RequestBody Product product, Model model) {
        logger.info(product);
        productHandler.saveProduct(product);
        model.addAttribute("product", product);

        return "/admin/adminNewProduct";
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.POST)
    public String singleProductUpdatePagePost(@PathVariable("id") Long id, @RequestBody Product product, Model model) {
        logger.info(product);
        productHandler.update(product);
        model.addAttribute("product", product);

        return "/admin/adminProduct";
    }
}