package ru.parfenov.project.dao.repository;

public class DaoLayerException extends RuntimeException {
    public DaoLayerException(String message) {
        super(message);
    }

    public DaoLayerException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoLayerException(Throwable cause) {
        super(cause);
    }
}
