package ru.parfenov.project.service.handlers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.parfenov.project.service.configuration.ServiceConfig;
import ru.parfenov.project.service.handlers.behaviors.CrudDaoBehavior;
import ru.parfenov.project.service.handlers.entity.Image;
import ru.parfenov.project.service.handlers.entity.Product;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = ServiceConfig.class)
@ExtendWith({MockitoExtension.class, SpringExtension.class})
public class ImagesHandlerTest {

    @Mock
    private CrudDaoBehavior saveBehavior;

    @InjectMocks
    private ImagesHandler imagesHandler;

    @Test
    @DisplayName("Тест на сохранение картинки")
    public void convertToServiceTest() {
        Image actual = getImage(1L);
        actual.setImageID(null);

        when(saveBehavior.save(any())).thenReturn(1L);

        imagesHandler.saveImage(actual);
        Image expected = getImage(1L);

        assertEquals(expected, actual);
    }

    private Image getImage(Long id) {
        Image image = new Image();
        image.setImageID(id);
        image.setPath("/img" + id);
        image.setProduct(getProduct(id));
        return image;
    }

    private Product getProduct(Long id) {
        Product product = new Product();
        product.setProductID(id);
        product.setName("ProductName" + id);
        product.setAnnotations("ProductAnnotation" + id);
        product.setDescription("ProductDescription" + id);
        product.setKeywords("ProductDescription" + id);
        product.setPrice(new BigDecimal(id));
        product.setDiscount(id.doubleValue());
        product.setIsActive(true);
        product.setIsPopular(true);
        return product;
    }

}
