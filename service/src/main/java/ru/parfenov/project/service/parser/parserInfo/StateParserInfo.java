package ru.parfenov.project.service.parser.parserInfo;

public enum StateParserInfo {
    NOT_STARTED,
    IN_PROGRESS,
    WAIT,
    DENY,
    END
}
