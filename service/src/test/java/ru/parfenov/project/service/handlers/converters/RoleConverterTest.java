package ru.parfenov.project.service.handlers.converters;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.parfenov.project.dao.model.RoleModel;
import ru.parfenov.project.service.configuration.ServiceConfig;
import ru.parfenov.project.service.handlers.entity.Role;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfig.class)
public class RoleConverterTest {

    @Autowired
    private RoleConverter roleConverter;

    @Test
    @DisplayName("Тест на конвертацию Role в Service модель")
    public void convertToServiceTest() {
        Role expected = getRole(1L);
        RoleModel roleModel = getRoleModel(1L);
        Role actual = roleConverter.convertToService(roleModel);

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Тест на конвертацию Role в Dao модель")
    public void convertToDaoTest() {
        RoleModel expected = getRoleModel(1L);
        Role role = getRole(1L);
        RoleModel actual = roleConverter.convertToDao(role);

        assertEquals(expected, actual);
    }

    private Role getRole(Long id) {
        Role role = new Role();
        role.setRoleID(id);
        role.setName("testRole" + id);
        role.setIsAdmin(false);
        return role;
    }

    private RoleModel getRoleModel(Long id) {
        RoleModel role = new RoleModel();
        role.setId(id);
        role.setName("testRole" + id);
        role.setIsAdmin(false);
        return role;
    }
}
