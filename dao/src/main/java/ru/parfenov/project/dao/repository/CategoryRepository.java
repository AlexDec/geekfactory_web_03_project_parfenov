package ru.parfenov.project.dao.repository;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ru.parfenov.project.dao.model.CategoryModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class CategoryRepository implements RepositoryEntity<CategoryModel, Long> {
    private final Logger logger = Logger.getLogger(ProductRepository.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CategoryModel findById(Long id) {
        CategoryModel categoryModel = entityManager.find(CategoryModel.class, id);
        return categoryModel;
    }

    @Override
    public List<CategoryModel> findAll() {
        List<CategoryModel> categories = entityManager.createQuery("FROM CategoryModel cm", CategoryModel.class)
                .getResultList();
        return categories;
    }

    @Override
    public void remove(Long id) {
        CategoryModel categoryModel = entityManager.find(CategoryModel.class, id);
        entityManager.remove(categoryModel);
    }

    @Override
    public Long save(CategoryModel model) {
        entityManager.persist(model);
        return model.getCategoryID();
    }

    @Override
    public void update(CategoryModel model) {
        entityManager.merge(model);
    }

    @Override
    public CategoryModel findByName(String name) {
        TypedQuery<CategoryModel> query = entityManager.createQuery("SELECT cm FROM CategoryModel cm WHERE cm.name = :name", CategoryModel.class);
        try {
            CategoryModel categoryModel = query.setParameter("name", name).getSingleResult();
            return categoryModel;
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }
}
