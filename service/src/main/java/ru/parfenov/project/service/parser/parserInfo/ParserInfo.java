package ru.parfenov.project.service.parser.parserInfo;

import lombok.Data;

import java.util.List;

@Data
public class ParserInfo {
    private List<ProductParserInfo> productsParserInfo;
    private StateParserInfo stateParserInfo;
}
