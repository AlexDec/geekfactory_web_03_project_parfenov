package ru.parfenov.project.service.parser.parserInfo;

public enum ProductStateParserInfo {
    NEW,
    UPDATED,
    UNDEFINED
}
