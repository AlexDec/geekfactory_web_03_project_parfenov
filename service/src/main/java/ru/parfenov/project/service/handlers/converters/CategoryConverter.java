package ru.parfenov.project.service.handlers.converters;

import org.springframework.beans.factory.annotation.Autowired;
import ru.parfenov.project.dao.model.CategoryModel;
import ru.parfenov.project.service.handlers.entity.Category;

public class CategoryConverter implements Converter<Category, CategoryModel> {

    @Autowired
    private CategoryConverter categoryConverter;

    @SuppressWarnings("Duplicates")
    @Override
    public Category convertToService(CategoryModel daoModel) {
        Category category = new Category();
        category.setCategoryID(daoModel.getCategoryID());
        category.setName(daoModel.getName());
        category.setCategoryURL(daoModel.getCategoryURL());
        category.setDescription(daoModel.getDescription());
        category.setIsActive(daoModel.getIsActive());
        category.setIsDiscount(daoModel.getIsDiscount());

        CategoryModel categoryModel = daoModel.getParentCategory();
        if (categoryModel != null) {
            Category parentCategory = categoryConverter.convertToService(categoryModel);
            category.setParentCategory(parentCategory);
        }

        return category;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public CategoryModel convertToDao(Category serviceModel) {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setCategoryID(serviceModel.getCategoryID());
        categoryModel.setName(serviceModel.getName());
        categoryModel.setCategoryURL(serviceModel.getCategoryURL());
        categoryModel.setDescription(serviceModel.getDescription());
        categoryModel.setIsActive(serviceModel.getIsActive());
        categoryModel.setIsDiscount(serviceModel.getIsDiscount());

        Category category = serviceModel.getParentCategory();
        if (category != null) {
            CategoryModel parentCategory = categoryConverter.convertToDao(category);
            categoryModel.setParentCategory(parentCategory);
        }

        return categoryModel;
    }
}
