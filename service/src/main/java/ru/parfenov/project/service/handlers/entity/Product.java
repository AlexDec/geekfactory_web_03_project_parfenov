package ru.parfenov.project.service.handlers.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Product implements ServiceModel {
    private Long productID;
    private String name;
    private String annotations;
    private String description;
    private String keywords;
    private BigDecimal price;
    private Double discount;
    private Boolean isActive;
    private Boolean isPopular;
    private String url;
    private List<Image> images;
    private List<Category> categoryID;
    private List<Property> properties;

}
