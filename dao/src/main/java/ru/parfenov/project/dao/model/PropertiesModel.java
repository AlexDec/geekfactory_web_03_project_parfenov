package ru.parfenov.project.dao.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_properties")
public class PropertiesModel implements DaoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long propertiesID;
    @Column
    private String name;
    @Column
    private String value;
}
