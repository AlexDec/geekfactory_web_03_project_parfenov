package ru.parfenov.project.service.handlers.behaviors;

import ru.parfenov.project.dao.model.DaoModel;
import ru.parfenov.project.dao.repository.RepositoryEntity;
import ru.parfenov.project.service.handlers.converters.Converter;
import ru.parfenov.project.service.handlers.entity.ServiceModel;

import java.util.LinkedList;
import java.util.List;

public class CrudDaoBehavior {
    private RepositoryEntity repository;
    private Converter<ServiceModel, DaoModel> converter;

    public CrudDaoBehavior(RepositoryEntity repository, Converter converter) {
        this.repository = repository;
        this.converter = converter;
    }

    public ServiceModel findById(Long id) {
        DaoModel daoModel = repository.findById(id);
        return converter.convertToService(daoModel);
    }

    public ServiceModel findByName(String name) {
        DaoModel daoModel = repository.findByName(name);
        if (daoModel != null) {
            return converter.convertToService(daoModel);
        } else {
            return null;
        }
    }

    public List<ServiceModel> findAll() {
        List<ServiceModel> resultList = new LinkedList<>();
        List<DaoModel> daoModelList = repository.findAll();

        for (DaoModel model : daoModelList) {
            resultList.add(converter.convertToService(model));
        }
        return resultList;
    }

    public Long save(ServiceModel serviceModel) {
        DaoModel daoModel = converter.convertToDao(serviceModel);
        Long id = repository.save(daoModel);
        return id;
    }

    public void update(ServiceModel serviceModel) {
        DaoModel daoModel = converter.convertToDao(serviceModel);
        repository.update(daoModel);
    }

    public void remove(ServiceModel serviceModel) {
        DaoModel daoModel = converter.convertToDao(serviceModel);
        repository.remove(daoModel);
    }
}
